# ncollect #



# overview

ncollect stats session to collect data from several nats subscription


# synoptic

## starts session
	
with input parameters

* list of subscriptions subject
* tiemout

## query session

* finished status
* fetch collected data by subject
* fetch all collected data

## close session

* close subscriptions
* return collected data


